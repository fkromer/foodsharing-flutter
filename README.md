# foodsharing-flutter

Client app for foodsharing.de implemented in Flutter.

Checkout the [wiki](https://gitlab.com/fkromer/foodsharing-flutter/-/wikis/home)
to learn about how to setup a Debian based system for Linux app development.

## Mockup

![Mockup Desktop](docs/gifs/mockup-desktop.gif)
