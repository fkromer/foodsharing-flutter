import 'package:bloc/bloc.dart';
import 'ScreenEvents.dart';
import 'ScreenState.dart';

class ScreenBloc extends Bloc<ScreenEvent, ScreenState> {
  ScreenBloc() : super(ScreenStateLogin()) {
    on<ScreenEventLogin>((event, emit) => emit(ScreenStateLogin()));
    on<ScreenEventDashboard>((event, emit) => emit(ScreenStateDashboard()));
    on<ScreenEventNotifications>((event, emit) => emit(ScreenStateNotifications()));
    on<ScreenEventMessages>((event, emit) => emit(ScreenStateMessages()));
    on<ScreenEventCompanyChats>((event, emit) => emit(ScreenStateCompanyChats()));
    on<ScreenEventBaskets>((event, emit) => emit(ScreenStateBaskets()));
    on<ScreenEventFairteiler>((event, emit) => emit(ScreenStateFairteiler()));
  }
}
