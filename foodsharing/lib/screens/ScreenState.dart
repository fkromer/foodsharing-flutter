
import 'package:meta/meta.dart';

@immutable
abstract class ScreenState {}

class ScreenStateLogin extends ScreenState {}

class ScreenStateDashboard extends ScreenState {}

class ScreenStateNotifications extends ScreenState {}

class ScreenStateMessages extends ScreenState {}

class ScreenStateCompanyChats extends ScreenState {}

class ScreenStatePickupSlots extends ScreenState {}

class ScreenStateBaskets extends ScreenState {}

class ScreenStateFairteiler extends ScreenState {}
