import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsharing/screens/BasketsView.dart';
import 'package:foodsharing/screens/DashboardView.dart';
import 'package:foodsharing/screens/PickupSlotsView.dart';
import 'package:foodsharing/screens/ScreenState.dart';
import 'CompanyChatsView.dart';
import 'Fairteiler.dart';
import 'LoginView.dart';
import 'MessagesView.dart';
import 'NotificationsView.dart';
import 'ScreenBloc.dart';
import 'ScreenEvents.dart';

class ScreenMasterView extends StatelessWidget {
  const ScreenMasterView({Key? key, required this.childScreen}) : super(key: key);

  final Widget childScreen;

  @override
  Widget build(BuildContext context) {
  return Row(
      children: [
        Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image.asset('assets/csm_Foodsharing_Logo_be34216355.png')
              ),
              Divider(
                height: 1,
                thickness: 1,
              ),
              ListTile(
                leading: Icon(Icons.login),
                title: Text('Login'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventLogin()),
              ),
              ListTile(
                leading: Icon(Icons.dashboard),
                title: Text('Dashboard'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventDashboard()),
              ),
              ListTile(
                leading: Icon(Icons.doorbell),
                title: Text('Benachrichtigungen'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventNotifications()),
              ),
              ListTile(
                leading: Icon(Icons.email),
                title: Text('Nachrichten'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventMessages()),
              ),
              ListTile(
                leading: Icon(Icons.chat),
                title: Text('Betriebschats'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventCompanyChats()),
              ),
              ListTile(
                leading: Icon(Icons.business),
                title: Text('Abholslots'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventPickupSlots()),
              ),
              ListTile(
                leading: Icon(Icons.shopping_basket),
                title: Text('Essenskörbe'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventBaskets()),
              ),
              ListTile(
                leading: Icon(Icons.shuffle),
                title: Text('Fairteiler'),
                onTap: () => context.read<ScreenBloc>().add(ScreenEventFairteiler()),
              ),
            ]
          )
        ),
        VerticalDivider(
          width: 1,
          thickness: 1,
        ),
        Expanded(
          child: Scaffold(
            appBar: AppBar(
              title: Text(
                'Foodsharing',
                style: Theme.of(context).primaryTextTheme.headline6
              ),
            ),
            body: Center(
              child: childScreen,
            )
          )
        )
      ]
    );
  }
}

Widget getScreen(BuildContext _, ScreenState state) {
  if (state is ScreenStateLogin) {
    return ScreenMasterView(childScreen: ScreenLoginView());
  }
  else if (state is ScreenStateDashboard) {
    return ScreenMasterView(childScreen: ScreenDashboardView());
  }
  else if (state is ScreenStateNotifications) {
    return ScreenMasterView(childScreen: ScreenNotificationsView());
  }
  else if (state is ScreenStateMessages) {
    return ScreenMasterView(childScreen: ScreenMessagesView());
  }
  else if (state is ScreenStateCompanyChats) {
    return ScreenMasterView(childScreen: ScreenCompanyChatsView());
  }
  else if (state is ScreenStatePickupSlots) {
    return ScreenMasterView(childScreen: ScreenPickupSlotsView());
  }
  else if (state is ScreenStateBaskets) {
    return ScreenMasterView(childScreen: ScreenBasketsView());
  }
  else if (state is ScreenStateFairteiler) {
    return ScreenMasterView(childScreen: ScreenFairteilerView());
  }
  else {
    return ScreenMasterView(childScreen: ScreenLoginView());
  }
}