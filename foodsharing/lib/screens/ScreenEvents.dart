import 'package:meta/meta.dart';

@immutable
abstract class ScreenEvent {}

class ScreenEventLogin extends ScreenEvent {}

class ScreenEventDashboard extends ScreenEvent {}

class ScreenEventNotifications extends ScreenEvent {}

class ScreenEventMessages extends ScreenEvent {}

class ScreenEventCompanyChats extends ScreenEvent {}

class ScreenEventPickupSlots extends ScreenEvent {}

class ScreenEventBaskets extends ScreenEvent {}

class ScreenEventFairteiler extends ScreenEvent {}
