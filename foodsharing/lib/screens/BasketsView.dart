import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class ScreenBasketsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
              options: MapOptions(
                center: LatLng(50.983334, 11.033333),  // Erfurt
                zoom: 6.0,
                interactiveFlags: InteractiveFlag.all - InteractiveFlag.rotate,
              ),
              layers: [
                TileLayerOptions(
                  urlTemplate:
                      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                  subdomains: ['a', 'b', 'c'],
                ),
                MarkerLayerOptions(
                    markers: [
                      Marker(
                        point: LatLng(  // Berlin
                          52.520008,
                          13.404954,
                        ),
              builder: (context) => const Icon(
                          Icons.shopping_basket,
                          color: Colors.red,
                          size: 20.0,
                        ),
                      ),
                      Marker(
                        point: LatLng(  // Ausgburg
                          48.366512,
                          10.894446,
                        ),
              builder: (context) => const Icon(
                          Icons.shopping_basket,
                          color: Colors.red,
                          size: 20.0,
                        ),
                      ),
                    ]
                )
              ]
    );
  }
}