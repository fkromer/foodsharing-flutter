import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsharing/screens/MasterView.dart';
import 'package:foodsharing/screens/LoginView.dart';
import 'package:foodsharing/screens/BasketsView.dart';
import 'screens/ScreenBloc.dart';
import 'screens/ScreenState.dart';

void main() {
  runApp(
    BlocProvider(
      create: (context) => ScreenBloc(),
      child: FoodsharingApp(),
    )
  );
}

class FoodsharingApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Foodsharing',
      theme: ThemeData(
        primarySwatch: Colors.brown,
        primaryTextTheme: TextTheme(
          headline6: TextStyle(
            color: Colors.green
          )
        ),
      ),
      home: BlocBuilder<ScreenBloc, ScreenState>(
        builder: (BuildContext _, ScreenState state) => getScreen(_, state)
      ),
    );
  }
}
